"""
Input: integer
Output: string specifying
    Chris's flies (1/2 rounded down),
    Tom's flies (total),
    Cat's total (Chris + tom)
"""

def frog_contest(flies):
    chris = sum(range(1, flies+1))
    tom = sum(range(1, (int(chris/2))+1))
    cat = sum(range(1, (int(chris + tom))+1))
    return f"Chris ate {chris} flies, Tom ate {tom} flies and Cat ate {cat} flies"

"""
Straightforward using the sum of ranges.
The time complexity is O(n) where n is the integer input.
"""

# flies = 5
# print(frog_contest(flies))
