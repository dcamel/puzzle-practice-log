def solution(s):
    final_s = ""
    for letter in s:
        if letter == s[0]:
            final_s += letter
            pass
        elif letter.isupper():
            final_s += " " + letter
        else:
            final_s += letter
    return final_s

s = "breakCamelCase"
print(solution(s))

"""
Easy string concatenation.
The time complexity is O(n) where n is the length of the string.
"""
