"""
Input: 2 lists of strings, one for answer Key, one for student repsonse
Output: True / False if either
    (1) all answers match entries that don't have "_"
    (2) all answers don't match entires that don't have "_"

Important Note: Questions with "_" are to be skipped.
"""

def possibly_perfect(key, answers):
    key_sum = len(key)
    status = None
    for num in range(0, key_sum):
        if key[num] == "_":
            pass
        elif status == None:
            status = key[num] == answers[num]
        else:
            temp = key[num] == answers[num]
            if status != temp:
                return False
    return True

"""
18 Apr 2014 - Solved with a range for loop.
Evaluates if temporary status (temp) of each iteration is the same as the original status.
The time-complexity of this is O(n) based on the length of key and answers list.
"""
