"""
input: string of words
output: string with words with %+ letter words reversed
note: only letters and spaces in string
"""

def spin_words(sentence):
    word_list = sentence.split(" ")
    final_string = ""
    for word in word_list:
        if len(word) >= 5:
#             final_string += ''.join(reversed(word)) + " "
            final_string += word[::-1] + " "
        else:
            final_string += word + " "
    return final_string[:-1]

"""
String slicing vs .join(reversed()).
I left the join/reversed in to review later.  I forgot the -d in reversed.
The time complexity is O(n) where n is the length of the string.
"""

# sentence = "Hi World and Worldette"
# print(spin_words(sentence))
