def generate_hashtag(s):
    if s == '':
        return False
    new_list = s.split(" ")
    final_string = "#"
    for word in new_list:
        if word == '':
            pass
        else:
            final_string += word.capitalize()
    if len(final_string) > 140:
        return False
    return final_string

"""
Here is a more efficient version posted.
Wasn't aware that .title() method existed.  Pretty useful.
"""

def generate_hashtag2(s):
    s = s.title()
    s = s.replace(" ", "")
    if len(s) < 140 and len(s) > 0:
        return "#" + s
    else:
        return False


s = '      Code wars'
#CodeWars
# s = 'Codewars is nice'
# #CodewarsIsNice
print(generate_hashtag(s))
print(generate_hashtag2(s))
