"""
Input: integer
Output: the number of occurances of '1' in the binary representation of the integer
"""

def count_bits(n):
    n_binary = str(bin(n)[2:])
    return n_binary.count('1')

n = 1234
print(count_bits(n))

"""
Looked up the bin() function.  Noticed there is a '0b' prefix that we don't need.
Used stringe-slice syntax to shorten the conversion, dropping the prefix.
The time complexity is O(log n + m),
    where n is the value of the integer n
    and m is the length of its binary representation.
"""
