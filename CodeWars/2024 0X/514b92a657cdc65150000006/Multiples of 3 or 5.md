https://www.codewars.com/kata/514b92a657cdc65150000006/train/python
# Instructions
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in.

Additionally, if the number is negative, return 0.

Note: If the number is a multiple of both 3 and 5, only count it once.

Courtesy of projecteuler.net (Problem 1)

# My Notes
# 2024 May 08 - Python ✅
Answered first with a range iterating over all numbers under the input.
Then, created two range's using their step counts for each 3 and 5.  With those values stored in new variables, combined the unique numbers in a set, which strips away duplicate numbers.
This approach uses more variables, but greatly reduces the total iteration cycles.
