"""
Input: string of letters
Output: T/F if word is an isogram (containing no repeating letters)
Special note: ignoring case of letter
"""

def is_isogram(string):
    chars = set()
    for letter in string.lower():
        if letter in chars:
            return False
        chars.add(letter)
    return True

"""
Used a set instead of a list.  They are optimized for membership testing with the 'in' operator.
Lowered the new variable count by moving the `.lower()` to the for loop line.
The time complexity is O(n) where n is the length of the string.
"""

# Solution testing:
# string = "Isogram"
# string = "isIsogram"
# print(is_isogram(string))

"""
Simplier solution provided on CodeWars using the a set's inability to add duplicate values.
"""
# def is_isogram(string):
#     return len(string) == len(set(string.lower()))
