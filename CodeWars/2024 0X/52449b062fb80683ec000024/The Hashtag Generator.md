# Instructions
The marketing team is spending way too much time typing in hashtags.
Let's help them with our own Hashtag Generator!

Here's the deal:

It must start with a hashtag (#).
All words must have their first letter capitalized.
If the final result is longer than 140 chars it must return false.
If the input or the result is an empty string it must return false.

Examples
    ```
    " Hello there thanks for trying my Kata"  =>  "#HelloThereThanksForTryingMyKata"
    "    Hello     World   "                  =>  "#HelloWorld"
    ""                                        =>  false
    ```

# My Notes
## 2024 May 18 - Python ✅
`.capitalize()` is a handy method for capitlizaing the first letter of a string.
By breaking the stream by a space delimiter, was able to use that to add to the final string.

There's likely a way to do the len() function at the beginning to determine if the final string is going to be over the limit.
Will have to research later. 
