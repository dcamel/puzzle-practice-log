def move_zeros(input):
    count_z = input.count(0)
    while 0 in input:
        input.remove(0)
    input.extend([0] * count_z)
    return input



input = [1, 2, 0, 1, 0, 1, 0, 3, 0, 1]
print(move_zeros(input))
