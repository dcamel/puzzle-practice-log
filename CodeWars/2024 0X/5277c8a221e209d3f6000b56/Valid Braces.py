"""
Input: string of braces (){}[] in any order.
Output: boolean T/F if the braces close properly or not
Notes: "([)])" is not valid, as they don't close properly
"""

def valid_braces(string):
    newlist = []
    count = 0
    for brace in string:
        if brace == "(" or brace == "{" or brace == "[":
            newlist.append(brace)
            count += 1
        elif brace == ")":
            if count == 0 or newlist[count - 1] != "(":
                return False
            else:
                newlist.pop()
                count -= 1
        elif brace == "}":
            if count == 0 or newlist[count - 1] != "{":
                return False
            else:
                newlist.pop()
                count -= 1
        elif brace == "]":
            if count == 0 or newlist[count - 1] != "[":
                return False
            else:
                newlist.pop()
                count -= 1
    return count == 0

string = "[]"
print(valid_braces(string))


"""
This works, but isn't efficient at all.
Unfortunately, did not arrive at the more optimal solution, but will not forget it.
See below:
"""
# I wish I thought of this, lol.

# def validBraces(string):
#     while '()' in string or '{}' in string or '[]' in string:
#         string = string.replace('()', '')
#         string = string.replace('{}', '')
#         string = string.replace('[]', '')
#     return False if len(string) != 0 else True
