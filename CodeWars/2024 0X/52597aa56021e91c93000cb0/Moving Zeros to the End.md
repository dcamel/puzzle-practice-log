52597aa56021e91c93000cb0

# Instructions
Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

    ```
    move_zeros([1, 0, 1, 2, 0, 1, 3]) # returns [1, 1, 2, 1, 3, 0, 0]
    ```

## My Notes:
## 2024 May 013 - Python ✅
Had to look up why .remove() method wasn't removing all 0's.  Forgot that it only removes the first one, requiring a while loop.
Second mystery was why I couldn't just return the last line.  Looked it up and using .extend() method returns a None for its line.

One of the code wars examples had this:
    ```
    def move_zeros(input):
        non_zero_elements = [num for num in input if num != 0]
        zero_count = input.count(0)
        return non_zero_elements + [0] * zero_count

    input = [1, 2, 0, 1, 0, 1, 0, 3, 0, 1]
    print(move_zeros(input))  # Output: [1, 2, 1, 1, 3, 1, 0, 0, 0, 0]
    ```

Didn't know you could just do that with a + and another list.  Pretty clever.
