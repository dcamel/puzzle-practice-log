https://www.codewars.com/kata/65112af7056ad6004b5672f8/train/python

# Instructions
Suppose a student can earn 100% on an exam by getting the answers all correct or all incorrect. Given a potentially incomplete answer key and the student's answers, write a function that determines whether or not a student can still score 100%. Incomplete questions are marked with an underscore, "_".

## Possible for student to get all questions correct.
    ["A", "_", "C", "_", "B"]   # answer key
    ["A", "D", "C", "E", "B"]   # student's solution
    ➞ True

## First question is correct but third is wrong, so not possible to score 100%.
    ["B", "_", "B"]   # answer key
    ["B", "D", "C"]   # student's solution
    ➞ False

## Possible for student to get all questions incorrect.
    ["T", "_", "F", "F", "F"]   # answer key
    ["F", "F", "T", "T", "T"]   # student's solution
    ➞ True

## Examples
    ```
    (["B", "A", "_", "_"], ["B", "A", "C", "C"]) ➞ True
    (["A", "B", "A", "_"], ["B", "A", "C", "C"]) ➞ True
    (["A", "B", "C", "_"], ["B", "A", "C", "C"]) ➞ False
    (["B", "_"], ["C", "A"]) ➞ True
    (["B", "A"], ["C", "A"]) ➞ False
    (["B"], ["B"]) ➞ True
    (["_"], ["B"]) ➞ True
    ```
## Notes
    Test has at least one question.
    len(key) == len(answers)


# My notes

## 2024 Apr 18 - Python ☑

Originally, I did not know to skip the "_" answers and marked as "True"
The following would work under that consideration:
    ```
    def possibly_perfect(key, answers):

        print(key)
        print(answers)
        print("------------")
        orig_status = key[0] == answers[0] or key[0] == "_"
        key_sum = len(key)
        for num in range(1, key_sum):
            temp = key[num] == answers[num] or key[num] == "_"
            print(temp)
            if orig_status != temp:
                return False
        return True
    ```
