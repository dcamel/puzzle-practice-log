def queue_time(customers, n):
    highest_time = 0
    for i in range(len(customers)):
        highest_time = max(highest_time, sum(customers[:i+1]))
    # for i in range(n - len(customers)):
    #     highest_time = max(highest_time, sum(customers))
    return highest_time
