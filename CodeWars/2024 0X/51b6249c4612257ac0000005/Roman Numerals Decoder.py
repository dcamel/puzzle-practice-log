"""
Input: string of roman numerals
Output: the converted integer value of the string
"""

def solution(roman : str) -> int:
    total = 0
    # M 1000
    total += (900 * roman.count('CM'))
    roman = roman.replace('CM', '')
    total += (1000 * roman.count('M'))
    # D 500
    total += (400 * roman.count('CD'))
    roman = roman.replace('CD', '')
    total += (500 * roman.count('D'))
    # C 100
    total += (90 * roman.count('XC'))
    roman = roman.replace('XC', '')
    total += (100 * roman.count('C'))
    # L 50
    total += (40 * roman.count('XL'))
    roman = roman.replace('XL', '')
    total += (50 * roman.count('L'))
    # X 10
    total += (9 * roman.count('IX'))
    roman = roman.replace('IX', '')
    total += (10 * roman.count('X'))
    # V 5
    total += (4 * roman.count('IV'))
    roman = roman.replace('IV', '')
    total += (5 * roman.count('V'))
    # I 1
    total += (1 * roman.count('I'))

    return total

# roman = 'MMMCMXCIX'
# # equals 3999
# roman = 'MMMDCCCLXXXVIII'
# # equals 3888
# print(solution(roman))

"""
I opted to remove only the combinations that would mess up the count, such as 'CM', 'CD', 'XC', etc.
Time complexity is O(n), where n is the length of the input roman string.
"""
