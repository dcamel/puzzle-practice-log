https://www.codewars.com/kata/54ba84be607a92aa900000f1/train/python

# Instructions

An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.

Example: (Input --> Output)

"Dermatoglyphics" --> true "aba" --> false "moOse" --> false (ignore letter case)

    ```
    isIsogram "Dermatoglyphics" = true
    isIsogram "moose" = false
    isIsogram "aba" = false
    ```

# My Notes

## 2024 Apr 24 - Python ☑
Straightforward and solved.

When comparing to see what others did, one user uploaded this solution:

    ```
    def is_isogram(string):
        return len(string) == len(set(string.lower()))
    ```

Extremely clever.  Sets cannot store duplicate elements.
Very clever.
