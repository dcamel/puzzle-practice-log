https://www.codewars.com/kata/5264d2b162488dc400000001/train/python

# Instructions
Write a function that takes in a string of one or more words, and returns the same string, but with all words that have five or more letters reversed (Just like the name of this Kata). Strings passed in will consist of only letters and spaces. Spaces will be included only when more than one word is present.

Examples:
    ```
    "Hey fellow warriors"  --> "Hey wollef sroirraw"
    "This is a test        --> "This is a test"
    "This is another test" --> "This is rehtona test"
    ```

# My Notes
## 2024 Apr 28 - Python
String slice syntax to the rescue again.
Couldn't remember empty-string-dot-joins-the-reversed.  
