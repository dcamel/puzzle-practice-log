"""
input: integer
output: integer containing the square (^2) of each single integer, concatenated together
"""

def square_digits(num):
    newstring = str(num)
    result = ""
    for item in newstring:
        result += str(int(item)**2)
    return int(result)


"""
Very straightforward.
Converted each integer to a string, iterated the string, and squared each number.
Result was converted back to string for appending.  Result returned as integer.
The time complexity is O(n) where n is the length of the string.
"""

"""
This can be further reduced by eliminating the first variable.
Using the `str(num)` in the for-loop line.
"""

# def square_digits(num):
#     result = ""
#     for item in str(num):
#         result += str(int(item)**2)
#     return int(result)
