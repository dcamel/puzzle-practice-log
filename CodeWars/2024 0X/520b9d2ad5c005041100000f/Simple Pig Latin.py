"""
Input: string sentence
Output: string sentence converted to pig latin with punctuation untouched.
Note: punct. is only at the end of the sentence
"""

def pig_it(text):
    new_s = text.split(" ")
    final_string = ""
    for s in new_s:
        if len(s) > 1:
            final_string += (s[1:] + s[0] + "ay" + " ")
        elif len(s) == 1 and s.isalpha():
            final_string += (s + "ay" + " ")
        else:
            final_string += (s)
    if final_string[-1] == " ":
        return final_string[:-1]
    return final_string

"""
Used stringe-slice syntax to shorten the code.
Instead of using a count to determine the end of the sentence without punct,
used a condition to check if the final character is a space" ".
The time complexity is O(n) where n is the length of the string.
"""
# text = "Hello World"
# print(pig_it(text))
