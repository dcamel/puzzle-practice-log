"""
Input: integer
Output: sum of all multiples of 3 or 5 below and/or equal to the integer
Note: if the integer is negative, return 0
"""

def solution(number):
    if number < 0:
        return 0
    mults = 0
    for num in range(3, number):
        if num % 3 == 0 or num % 5 == 0:
            mults += num
    return mults

number = 20
# answer should be 78
print(solution(number))

"""
Time complexity: O(n)
Straightforward.  Could reduce the amount of for loop processing for larger numbers though:
"""

def solution2(number2):
    if number2 < 0:
        return 0
    all_threes = list(range(3, number2, 3))
    all_fives = list(range(5, number2, 5))
    return sum(set(all_threes + all_fives))

number2 = 20
# answer should be 78
print(solution2(number2))

"""
Time complexity: O(n)
Way less processing for larger numbers.  Using the set() function to remove duplicates.
"""
