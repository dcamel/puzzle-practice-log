https://www.codewars.com/kata/520b9d2ad5c005041100000f/train/python

# Instructions
Move the first letter of each word to the end of it, then add "ay" to the end of the word. Leave punctuation marks untouched.

Examples:
    ```
    pig_it('Pig latin is cool') # igPay atinlay siay oolcay
    pig_it('Hello world !')     # elloHay orldway !
    ```

# My Notes
## 2024 Aug 24 - Python ✅
Forgot the rules for Pig Latin, but looked it up.  Included handling of 1letter words like "I" and "a".
There's likely a way to reduce this further.  Had to remember `.isalpha()`
