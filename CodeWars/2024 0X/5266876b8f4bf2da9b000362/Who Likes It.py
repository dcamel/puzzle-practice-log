"""
Input: Array of strings
Output: conditional string containing the following
    Empty string: "no one likes this"
    1 item string: "NameX likes this"
    2 item string: "NameX and NameY likes this"
    3 item string: "NameX, NameY and NameZ likes this"
    4+ item string: "Name X, NameY and # others like this"
"""

def likes(names):
    liked_total = len(names)
    match liked_total:
        case 0:
            return "no one likes this"
        case 1:
            return f"{names[0]} likes this"
        case 2:
            return f"{names[0]} and {names[1]} like this"
        case 3:
            return f"{names[0]}, {names[1]} and {names[2]} like this"
        case other:
            return f"{names[0]}, {names[1]} and {liked_total - 2} others like this"

"""
2024 Apr 18 - solved with a match-case statement.
Used match-case statements to ease the readability.
Used f-string for string interpoloation.

The time-complexity of this is O(1) based on the length of names list.
"""
