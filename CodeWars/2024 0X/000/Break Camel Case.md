https://www.codewars.com/kata/5208f99aee097e6552000148/train/python

# Instructions
Complete the solution so that the function will break up camel casing, using a space between words.

Example
    ```
    "camelCasing"  =>  "camel Casing"
    "identifier"   =>  "identifier"
    ""             =>  ""
    ```

# My Notes
## 2024 May 09 - Python ✅
Got it.  I think GitLab counts updates from 11pm EST as the next day.  My stats are probably scewed.
